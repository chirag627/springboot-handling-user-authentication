package com.app;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import com.app.entity.User;
import com.app.repository.UserRepository;

@SpringBootTest
class UserAuthenticationApplicationTests {

	@Autowired
	ApplicationContext context;

	@Test
	void contextLoads() {
	}

	// Adding User in database
	@Test
	void customerSignup() {
		UserRepository repo = context.getBean(UserRepository.class);
		User user = new User();
		user.setFname("chirag");
		user.setLname("Agarwal");
		user.setEmail("chiragagrawal123@gmail.com");
		user.setPassword("12341234");
		repo.save(user);

		System.out.println("Account created successfully");
	}

	// User Authentication
	@Test
	void customerLogin() {
		UserRepository repo = context.getBean(UserRepository.class);
		User user = new User();
		user.setEmail("chiragagrawal627@gmail.com");
		user.setPassword("12341234");
		List<User> isValidate = repo.findByEmailAndPassword(user.getEmail(), user.getPassword());
		if (isValidate.isEmpty()) {
			System.out.println("No account found for the email and password");
		} else {
			System.out.println("User found for the email and password. Login Successfull");
		}
	}

}
