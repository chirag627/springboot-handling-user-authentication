use mydb;

create table user(
id int,
fname varchar(50) not null,
lname varchar(50) not null,
email varchar(50) unique not null,
password varchar(50) not null
);

ALTER TABLE user
CHANGE COLUMN id id INT NOT NULL AUTO_INCREMENT ,
ADD PRIMARY KEY (`id`, `email`);