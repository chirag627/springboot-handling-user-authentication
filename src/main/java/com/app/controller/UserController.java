package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.entity.User;
import com.app.service.UserService;

@Controller
class UserController {

	@Autowired
	private UserService service;

	@RequestMapping(value = "/auth")
	public String authScreen() {
		return "auth";
	}

	@RequestMapping(value = "/auth/check", method = RequestMethod.POST)
	public String authCheck(ModelMap model, @RequestParam(name = "email") String email,
			@RequestParam(name = "password") String password) {
		List<User> isVerified = service.userAuthenticate(email, password);
		if (isVerified.isEmpty()) {
			model.addAttribute("error", "Invalid email and password. No account exists with the email and password");
			return "auth";
		} else {
			model.addAttribute("USER_DETAILS",isVerified);
			return "dashboard";
		}
	}
}
