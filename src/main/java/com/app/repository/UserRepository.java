package com.app.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.app.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	List<User> findByEmailAndPassword(String email, String password);
}
