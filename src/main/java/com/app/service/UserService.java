package com.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.app.entity.User;
import com.app.repository.UserRepository;

@Component
public class UserService {

	@Autowired
	private UserRepository repo;

	public List<User> userAuthenticate(String email, String password) {
		List<User> auth = repo.findByEmailAndPassword(email, password);
		return auth;

	}

}
