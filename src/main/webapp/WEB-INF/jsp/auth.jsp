<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Login | SportyShoes.com</title>
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Roboto">

<style>
body, h1, h2, h3, h4, h5, h6 {
	font-family: Roboto, sans-serif;
}

.inputbox {
	height: 20px;
	border: 1px solid #4f4f4f;
	border-radius: 10px;
	padding: 10px;
	width: 300px;
}

.inputbtn {
	background: black;
	color: white;
	padding: 10px;
	border-radius: 10px;
	width: 320px;
}

.center {
	margin: auto;
	vertical-align: middle;
	max-width: 300px;
	margin-top: 150px
}
</style>

</head>

<body>
	<div class="center">
		<img src="https://cdn-icons-png.flaticon.com/128/919/919275.png"
			data-src="https://cdn-icons-png.flaticon.com/128/919/919275.png"
			alt="Shoe " title="Shoe " width="50" height="50"
			class="display:inline-block;
			">
		<h4>Sign in to your account</h4>
		<!-- Cannot Login -->
		<%
		if (request.getAttribute("error") != null) {
		%>
		<p style="color: red"><%=request.getAttribute("error")%></p>
		<%
		}
		%>

		<!-- login form -->
		<form action="/auth/check" method="post">
			<input class="inputbox" name="email" type="email" required
				placeholder="Email Address"> <br /> <br /> <input
				class="inputbox" type="password" name="password" required
				placeholder="Password"> <br /> <br /> <input type="submit"
				name="submit" value="Login" class="inputbtn">
		</form>
		<!-- Login Form close-->

		<h5 style="color: #4f4f4f;">
			Forgot Password ? <br /> <br /> <a href="" style="color: #393f81">Don't
				have an account. Create one</a>
		</h5>
	</div>
</body>

</html>